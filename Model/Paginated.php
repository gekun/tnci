<?php

namespace AzureSpring\Tnci\Model;

interface Paginated
{
    public function getData(): array;
    public function getTotal(): int;
}
