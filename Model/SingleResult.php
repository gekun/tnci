<?php

namespace AzureSpring\Tnci\Model;

use AzureSpring\Tnci\Annotation\Template;

/** @Template({"T"}) */
class SingleResult extends Result
{
    private $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
