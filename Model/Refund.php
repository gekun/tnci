<?php

namespace AzureSpring\Tnci\Model;

class Refund
{
    const STATUS_USED = 1;
    const STATUS_NEW = 2;
    const STATUS_DONE = 3;
    const STATUS_REJECTED = 4;

    /** @var int */
    private $id;

    /** @var int */
    private $user;

    /** @var int */
    private $merchant;

    /** @var int */
    private $product;

    /** @var int */
    private $price;

    /** @var int */
    private $quantity;

    /** @var int */
    private $fee;

    /** @var int */
    private $status;

    /** @var \DateTimeImmutable */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getMerchant(): int
    {
        return $this->merchant;
    }

    /**
     * @return int
     */
    public function getProduct(): int
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getFee(): int
    {
        return $this->fee;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
