<?php

namespace AzureSpring\Tnci\Model;

class Order
{
    /** @var int */
    private $id;

    /** @var int */
    private $user;

    /** @var int */
    private $merchant;

    /** @var int|null */
    private $vendor;

    /** @var string */
    private $title;

    /** @var string */
    private $product;

    /** @var int */
    private $price;

    /** @var int */
    private $quantity;

    /** @var string */
    private $name;

    /** @var string */
    private $mobile;

    /** @var bool */
    private $sent;

    /** @var string|null */
    private $qrcode;

    /** @var string|null */
    private $code;

    /** @var string[] */
    private $codeVector = [];

    /** @var \DateTimeImmutable */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getMerchant(): int
    {
        return $this->merchant;
    }

    /**
     * @return int|null
     */
    public function getVendor(): ?int
    {
        return $this->vendor;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @return bool
     */
    public function isSent(): bool
    {
        return $this->sent;
    }

    /**
     * @return string|null
     */
    public function getQrcode(): ?string
    {
        return $this->qrcode;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string[]
     */
    public function getCodeVector(): array
    {
        return $this->codeVector;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
