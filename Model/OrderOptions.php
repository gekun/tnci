<?php

namespace AzureSpring\Tnci\Model;

class OrderOptions
{
    /** @var string|null */
    private $permanentId;

    /** @var string */
    private $name = '';

    /** @var string */
    private $mobile = '';

    /** @var string|null */
    private $idNumber;

    /** @var \DateTimeInterface|null */
    private $date;

    /** @var bool */
    private $paid = false;

    public static function create()
    {
        return new OrderOptions();
    }

    /**
     * @return string|null
     */
    public function getPermanentId(): ?string
    {
        return $this->permanentId;
    }

    /**
     * @param string|null $permanentId
     *
     * @return $this
     */
    public function setPermanentId(?string $permanentId): OrderOptions
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): OrderOptions
    {
        $this->name = preg_replace('/\p{S}/u', '', $name);

        return $this;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     *
     * @return $this
     */
    public function setMobile(string $mobile): OrderOptions
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    /**
     * @param string|null $idNumber
     *
     * @return $this
     */
    public function setIdNumber(?string $idNumber): self
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface|null $date
     *
     * @return $this
     */
    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     *
     * @return $this
     */
    public function setPaid(bool $paid): OrderOptions
    {
        $this->paid = $paid;

        return $this;
    }

    public function toParams(): array
    {
        return [ 'is_pay' => $this->isPaid() ? 1 : 0 ] + array_filter([
            'orders_id' => $this->getPermanentId(),
            'name' => $this->getName(),
            'mobile' => $this->getMobile(),
            'id_number' => $this->getIdNumber(),
            'start_date' => $this->getDate() ? $this->getDate()->format('Y-m-d') : null,
        ]);
    }
}
