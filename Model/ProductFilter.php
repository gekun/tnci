<?php

namespace AzureSpring\Tnci\Model;

class ProductFilter extends Paginator
{
    /**
     * @var array<int>
     */
    private $idVector = [];

    /**
     * @var string|null
     */
    private $keyword;

    /**
     * @var int|null
     */
    private $category;

    /**
     * @var int|null
     */
    private $district;

    static function create()
    {
        return new ProductFilter();
    }

    /**
     * @return array
     */
    public function getIdVector(): array
    {
        return $this->idVector;
    }

    /**
     * @param array $idVector
     *
     * @return $this
     */
    public function setIdVector(array $idVector): self
    {
        $this->idVector = $idVector;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @param string|null $keyword
     *
     * @return $this
     */
    public function setKeyword(?string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }

    /**
     * @param int|null $category
     *
     * @return $this
     */
    public function setCategory(?int $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDistrict(): ?int
    {
        return $this->district;
    }

    /**
     * @param int|null $district
     *
     * @return $this
     */
    public function setDistrict(?int $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function toParams(): array
    {
        return array_filter(parent::toParams() + [
            'cate_id' => $this->getCategory(),
            'zone' => $this->getDistrict(),
            'item_id' => implode(',', $this->getIdVector()),
            'key_word' => $this->getKeyword(),
        ]);
    }
}
