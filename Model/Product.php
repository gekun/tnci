<?php

namespace AzureSpring\Tnci\Model;

class Product
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var bool */
    private $online;

    /** @var bool */
    private $idNumberMandatory;

    /** @var int */
    private $quantity;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isOnline(): bool
    {
        return $this->online;
    }

    /**
     * @return bool
     */
    public function isIdNumberMandatory(): bool
    {
        return $this->idNumberMandatory;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
