<?php

namespace AzureSpring\Tnci\Notification;

class VerificationNotification extends OrderNotification
{
    /** @var int */
    private $quantity;

    public static function support(array $params)
    {
        return !array_diff(['another_orders_id', 'my_orders_id', 'amount', 'amount_used', 'code'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self( $params['my_orders_id'], $params['amount_used']);
    }

    public function __construct(string $orderId, int $quantity)
    {
        parent::__construct($orderId);

        $this->quantity = $quantity;  
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
