<?php

namespace AzureSpring\Tnci\Notification;

abstract class AbstractNotification
{
    abstract public static function support(array $params);
    abstract public static function compose(array $params);
}
