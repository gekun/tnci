<?php

namespace AzureSpring\Tnci\Notification;

class RefundNotification extends OrderNotification
{
    const STATE_APPROVED = 3;
    const STATE_REJECTED = 4;

    /** @var string */
    private $referenceId;

    /** @var bool */
    private $approved;

    /** @var int */
    private $quantity;

    /** @var float */
    private $subtotal;

    /** @var string */
    private $reason;

    public static function support(array $params)
    {
        return !array_diff(['amount', 'orders_id', 'serial_no', 'my_orders_id', 'type', 'message'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self(
            $params['my_orders_id'],
            $params['serial_no'],
            self::STATE_APPROVED === (int) $params['type'],
            $params['amount'],
            $params['message']
        );
    }

    public function __construct(string $orderId, string $referenceId, bool $approved, int $quantity, string $reason)
    {
        parent::__construct($orderId);

        $this->referenceId = $referenceId;
        $this->approved = $approved;
        $this->quantity = $quantity;
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }


    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }
}
